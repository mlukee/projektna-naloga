import tkinter as tk
from math import *
from tkinter import *
from tkinter import ttk
from tkinter import filedialog

from main import *

calculation = ""
izpis = ""
stevec = 0
def dodaj_v_racun(symbol):
    global calculation
    global izpis
    sqrt = "sqrt"
    global stevec
    dolzina = len(calculation)
    if symbol == "+":
     calculation += symbol 
     izpis = izpis + (" " + symbol + " ") #dam presledke med operacijami, da je lazje berljivo           
    elif symbol =="-":
      izpis = izpis + (" " + symbol + " ")
      calculation += symbol          
    elif symbol == "/":
      izpis = izpis + (" " + symbol + " ") 
      calculation += symbol          
    elif symbol ==  "*":
      izpis = izpis + (" " + symbol + " ") 
      calculation += symbol
    elif symbol == "√" :
      izpis = izpis + (symbol + " ")
      if dolzina == 0:
          pocisti()
          izracun.insert(1.0, "Error")          
      else:    
        calculation = calculation[:(dolzina-3)] + str(sqrt) + calculation[(dolzina-3):] #pred stevilko dodam sqrt 
    elif symbol == "^":
        izpis = izpis + symbol
        calculation = calculation + "**"
    elif symbol == "(":
        izpis = izpis + symbol
        calculation+="("
    elif symbol == ")":
        izpis = izpis + ( symbol + " ")
        calculation+=")" 
    elif symbol == "%":
        izpis = izpis + symbol
        calculation = calculation[:(dolzina-1)] + str("%") + calculation[(dolzina-1):] #pred stevilko 
        stevec=stevec+1       
    else:
        if stevec == 1: 
            izpis = izpis + ( symbol + " ")   
            calculation = calculation[:(dolzina-1)] + symbol + calculation[(dolzina-1):] #dodam znak za % 
            stevec = 0
        elif dolzina == 0:
            izpis = izpis + symbol
            calculation = calculation + ("("+ symbol + ")")
        elif calculation[-1] == ")":
            izpis = izpis + symbol
            calculation = calculation[:(dolzina-1)] + symbol + calculation[(dolzina-1):] #dodam pred oklepaj stevilko    
        else:
            izpis = izpis + symbol   
            calculation = calculation + ("("+ symbol + ")")
    
    print(calculation)
    izracun.delete(1.0, "end")
    izracun.insert('end', izpis, 'tag-right')
    
def dodajSt(symbol):
    global calculation
    global izpis
    # print(calculation)
    izpis += str(symbol)
    calculation += str(symbol)
    text_result.delete(1.0, "end")
    text_result.insert('end', izpis, 'tag-right')    

def izracunaj():
    global calculation
    global izpis
    racun =str(izpis) + " ="
    print("To je pred izracunom: "+ calculation)
    try:
        calculation = str(round(eval(calculation),5))
        izracunano.insert(1.0,racun, 'tag-right')
        izracun.delete(1.0, "end")
        izracun.insert('end', calculation, 'tag-right')
    except:
        pocisti()
        izracun.insert(1.0,"Error")

root = Tk()
root.title("Kalkulator")
root.geometry("375x350")

ikona = PhotoImage(file='calculator_icon.png')
root.iconphoto(True, ikona)

moj_okvir = ttk.Notebook(root)
moj_okvir.pack(pady=5)
osnovniKalkulator = Frame(moj_okvir, width=400, height=450, bg ="gray")
logicniOperatorji = Frame(moj_okvir, width=400, height=450, bg ="gray")
deloZDatotekami = Frame(moj_okvir, width=400, height=400, bg="gray")

osnovniKalkulator.pack(fill="both", expand=1)
logicniOperatorji.pack(fill="both", expand=1)
deloZDatotekami.pack(fill="both",expand=1)

moj_okvir.add(osnovniKalkulator, text="Kalkulator")
moj_okvir.add(logicniOperatorji, text="Logični operatorji")
moj_okvir.add(deloZDatotekami, text="Branje iz datotek")

#================================================ OSNOVNI KALKULATOR =============================================================
izracunano = Text(osnovniKalkulator, height=1, width=24, font=("Arial", 20))
izracun = Text(osnovniKalkulator, height=1, width=20, font=("Arial", 24))
izracun.tag_configure('tag-right', justify='right')
izracunano.grid(row=1,columnspan=7)
izracunano.tag_configure('tag-right', justify='right')
izracun.grid(row=2,columnspan=7)

btn_eksponent = Button(osnovniKalkulator, text="^", command=lambda: dodaj_v_racun('^'), width=5, font=("Arial", 14), bg="gray", fg="black")
btn_eksponent.grid(row=3, column = 2)
btn_eksponent = Button(osnovniKalkulator, text="%", command=lambda: dodaj_v_racun('%'), width=5, font=("Arial", 14), bg="gray", fg="black")
btn_eksponent.grid(row=3, column = 3)
btn_koren = tk.Button(osnovniKalkulator, text="√", command=lambda: dodaj_v_racun('√'), width=5, font=("Arial", 14), bg="gray", fg="black")
btn_koren.grid(row=3, column = 1)
btn_1 = tk.Button(osnovniKalkulator, text="1", command=lambda: dodaj_v_racun('1'), width=5, font=("Arial", 14))
btn_1.grid(row=4, column = 1)
btn_2 = tk.Button(osnovniKalkulator, text="2", command=lambda: dodaj_v_racun('2'), width=5, font=("Arial", 14))
btn_2.grid(row=4, column = 2)
btn_3 = tk.Button(osnovniKalkulator, text="3", command=lambda: dodaj_v_racun('3'), width=5, font=("Arial", 14))
btn_3.grid(row=4, column = 3)
btn_plus = tk.Button(osnovniKalkulator, text="+", command=lambda: dodaj_v_racun('+'), width=5, font=("Arial", 14), bg="orange", fg="white")
btn_plus.grid(row=4, column = 4)
btn_4 = tk.Button(osnovniKalkulator, text="4", command=lambda: dodaj_v_racun('4'), width=5, font=("Arial", 14))
btn_4.grid(row=5, column = 1)
btn_5 = tk.Button(osnovniKalkulator, text="5", command=lambda: dodaj_v_racun('5'), width=5, font=("Arial", 14))
btn_5.grid(row=5, column = 2)
btn_6 = tk.Button(osnovniKalkulator, text="6", command=lambda: dodaj_v_racun('6'), width=5, font=("Arial", 14))
btn_6.grid(row=5, column = 3)
btn_minus = tk.Button(osnovniKalkulator, text="-", command=lambda: dodaj_v_racun('-'), width=5, font=("Arial", 14), bg="orange", fg="white")
btn_minus.grid(row=5, column = 4)
btn_7 = tk.Button(osnovniKalkulator, text="7", command=lambda: dodaj_v_racun('7'), width=5, font=("Arial", 14))
btn_7.grid(row=6, column = 1)
btn_8 = tk.Button(osnovniKalkulator, text="8", command=lambda: dodaj_v_racun('8'), width=5, font=("Arial", 14))
btn_8.grid(row=6, column = 2)
btn_9 = tk.Button(osnovniKalkulator, text="9", command=lambda: dodaj_v_racun('9'), width=5, font=("Arial", 14))
btn_9.grid(row=6, column = 3)
btn_mnozenje = tk.Button(osnovniKalkulator, text="*", command=lambda: dodaj_v_racun('*'), width=5, font=("Arial", 14), bg="orange", fg="white")
btn_mnozenje.grid(row=7, column = 4)
btn_leviOklepaj = tk.Button(osnovniKalkulator, text="(", command=lambda: dodaj_v_racun('('), width=5, font=("Arial", 14))
btn_leviOklepaj.grid(row=7, column = 1)
btn_0 = tk.Button(osnovniKalkulator, text="0", command=lambda: dodaj_v_racun('0'), width=5, font=("Arial", 14))
btn_0.grid(row=7, column = 2)
btn_desniOklepaj = tk.Button(osnovniKalkulator, text=")", command=lambda: dodaj_v_racun(')'), width=5, font=("Arial", 14))
btn_desniOklepaj.grid(row=7, column = 3)
btn_deljenje = tk.Button(osnovniKalkulator, text="/", command=lambda: dodaj_v_racun('/'), width=5, font=("Arial", 14), bg="orange", fg="white")
btn_deljenje.grid(row=6, column = 4)
btn_izracunaj = tk.Button(osnovniKalkulator, text="=", command=izracunaj, width=13, font=("Arial", 14), bg="orange", fg="white")
btn_izracunaj.grid(row=8, column = 1, columnspan=2)
btn_pocisti = tk.Button(osnovniKalkulator, text="C", command=lambda: pocisti(), width=5, font=("Arial", 14))
btn_pocisti.grid(row=8, column = 3)